let curQuestion;
let numberOfQuestions;
let rightAnswers;
let listWithAnswers;

function init() {
	curQuestion = 1;
	numberOfQuestions = 10;
	rightAnswers = 0;
	listWithAnswers = new Array(numberOfQuestions);
	$('.questionForm').hide();
	$('#q1').show();
	$('#question-counter').html('Question ' + curQuestion + '/' + numberOfQuestions);
	document.getElementById("submit-button").style.display = "none";
	document.getElementById("warning").style.display = "none";
}

function goToNextQuestion() {

	if(getCurrentQuestionValue() == null) {
		alert("Do not leave blank");
	} else {
		processQuestion();
		$('.questionForm').hide();
		curQuestion++;

		let question = '#q' + curQuestion;
		$(question).show();

		$('#question-counter').html('Question ' + curQuestion + '/' + numberOfQuestions);

		if(curQuestion > numberOfQuestions) {
			document.getElementById("next-button").style.display = "none";
			document.getElementById("submit-button").style.display = "block";
			document.getElementById("warning").style.display = "block";
			$('#question-counter').html('End of the quiz');
		}
	
	}

}

function goToPreviousQuestion() {
	$('.questionForm').hide();
	document.getElementById("next-button").style.display = "block";
	document.getElementById("warning").style.display = "none";
	document.getElementById("submit-button").style.display = "none";
	curQuestion--;

	if(curQuestion < 1) curQuestion = 1;

	let question = '#q' + curQuestion;
	$(question).show();

	$('#question-counter').html('Question ' + curQuestion + '/' + numberOfQuestions);
}

function submitQuiz() {
	rightAnswers = 0;

	for(let i = 0; i < numberOfQuestions; i++) {
		let val = listWithAnswers[i];
		if(val == "true") rightAnswers++;
	}

	showEndScreen();
}

function processQuestion() {
	let question = 'input[name = q' + curQuestion + ']:checked';
	let answer = $(question).val();

	listWithAnswers[curQuestion - 1] = answer;
}

function getCurrentQuestionValue() {
	let question = 'input[name = q' + curQuestion + ']:checked';
	let val = $(question).val();

	return val;
}

function showEndScreen() {
	document.getElementById("back-button").style.display = "none";
	document.getElementById("next-button").style.display = "none";
	document.getElementById("submit-button").style.display = "none";
	document.getElementById("warning").style.display = "none";
	$('#question-counter').html('End of the quiz');
	$('#results').html('<h3>Your final score is ' + rightAnswers + ' out of ' + numberOfQuestions + '</h3><a href = "quiz.html">Take Quiz Again</a>');
}